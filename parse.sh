#!/bin/bash
function offerFile() {
	local file_size=$(ls -lah $1 | awk '{ print $5}')
	echo "$file $file_size";
	echo "(m)ove, (r)emove, (e)xecute, (s)kip the file or (q)uit"
}

function getAction() {
	local action
	read action
	echo $action;
}

function goodbye() {
	echo "Goodbye!"
	exit
}

function process_files() {
	for file in $dir/*; do
		file_name=$(basename "$file")
		file_extension="${file_name##*.}"

		if [[ -d $file ]]; then
			continue
		fi

		message=$(offerFile $file)
		echo $message
		action=$(getAction $file)

		case $action in
			"r") rm $file
				;;
			"o")
				xdg-open $file
				#wait &&
				message=$(offerFile $file)
				echo $message
				action=$(getAction $file)
				;;
			"m")
				echo "Enter destination path: ($dir/$file_extension/)"
				read dest
				if [ -z "$dest" ]
					then
						mkdir $dir/$file_extension/
						mv $file $dir/$file_extension/
					else
						mv $file $dest
				fi
				;;
			"q")
				goodbye
		esac
	done;
}

IFS=$'\n'

if [ -z "$1" ]
	then
	    echo "No argument supplied"
	    exit
fi

dir=$1

process_files

goodbye